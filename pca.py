# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 13:29:35 2018

@author: Raphaël
"""


""" ACP normée"""



import numpy as np
from math import pi, sqrt, sin, cos
from random import random
import matplotlib.pyplot as plt



### Annex functions ###

def dot_product(vect1, vect2):
	"""vect1 and vect2 must have the same length."""
	result = 0
	for k in range(len(vect1)):
		result += vect1[k] * vect2[k]
	return result



### Reading file ###

# @@@



n = 20			# Number of individuals
nb_variables = 3

r = [[random() for j in range(nb_variables)] for i in range(n)]


### Initializing matrix ###

mu = []
sigma = []				# Not real sigma; actually contains sqrt(n)*sigma
for j in range(nb_variables):
	mu.append(sum([r[i][j] for i in range(n)]) / n)
	sigma.append(sqrt(sum([(r[i][j] - mu[j])**2 for i in range(n)])))

# Matrice normée
X = []
for i in range(n):
	line = [(r[i][j] - mu[j]) / sigma[j] for j in range(nb_variables)]
	X.append(line)

tXX = []
for j in range(nb_variables):
	line = []
	for l in range(nb_variables):
		line.append(sum([X[k][j] * X[k][l] for k in range(n)]))
	tXX.append(line)

"""
Alternative calcul

tXX = []
for j in range(nb_variables):
	line = []
	for l in range(nb_variables):
		line.append(sum([(r[i][j]-mu[j])*(r[i][l]-mu[l]) / (sigma[j]*sigma[l]) for i in range(n)]))
	tXX.append(line)
"""



### Calculating things ###

spectrum, eigenvectors = np.linalg.eig(tXX)		# Note: spectrum is sorted

#print(spectrum)
#print(eigenvectors)

# @@@ Show sprectrum
print("Part d'inertie : ", (spectrum[-1] + spectrum[-2]) / sum(spectrum))

# The two first principal components
u1, u2 = eigenvectors[-1], eigenvectors[-2]

# The list of couples obtained by projection on u1 and u2
pca = []
for i in range(n):
	pca.append([dot_product(X[i], u1), dot_product(X[i], u2)])



### Representation ###

lst_t = np.linspace(0, 2*pi, 1000)
circle_x = [cos(t) for t in lst_t]
circle_y = [sin(t) for t in lst_t]
plt.plot(circle_x, circle_y)

plt.plot([0,0], [-1,1], color='black')
plt.plot([-2,2], [0,0], color='black')


plt.xlabel("First Principal Component")
plt.ylabel("Second Principal Component")
#Enlever la graduation des axes
#
plt.grid(True)
plt.axis('equal')

for i, point in enumerate(pca):
	plt.text(point[0], point[1], i, color='#ff00ff', size=8)

plt.show()


