from random import randint
from math import log

def generate(n):
    """Generate a random n long word"""
    x = str()
    for i in range(n):
        x += str(randint(0,1))
    return x

def inverse(x):
    nx = str()
    for l in x:
        if l == '0':
            nx += '1'
        else:
            nx += '0'
    return nx


def psi(x):
    p=q=1
    for l in x:
        if l=='0':
            q+=p
        else :
            p+=q
    return p,q

def decode(p,q):
    x = str()
    while p!=q:
        if p>q:
            p-=q
            x+='1'
        else:
            q-=p
            x+='0'
    return x[::-1]


def f(n):
    b=bin(n+1)
    x=str()
    for i in b[3:len(b)]:
        x+=i
    return x,psi(x)

def compare(p,q):
    """Tell informations about classic coding or tree coding of p/q"""
    a=1+int(log(p,2))
    b=1+int(log(q,2))
    x = decode(p,q)
    print(a,b,a+b)
    print(len(x))

"""Compare """
t=d=c=int()
l_classic = list()
percent_win = list()
for n in range(1, 10000000):
    x,(p,q)=f(n)
    a=1+int(log(p,2))
    b=1+int(log(q,2))
    l=len(x)
    e=a+b-l
    if e>0:
        t+=1
    elif e==0:
        d+=1
    else:
        c+=1
        l_classic.append(e)
    percent_win.append(c/n)


print("Tree wins : ", t)
print("Draw : ", d)
print("Classic wins : ", c, sum(l_classic)/len(l_classic))

from matplotlib.pyplot import plot
plot([i/10000000 for i in range(1,10000000)], percent_win)



